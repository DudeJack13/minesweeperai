-------------------------
# MineSweeper Readme #
## Created by Jack Sylvester-Barnett ##
-------------------------
## Summary Of The Project ##

VIDEO OF PROJECT: https://youtu.be/cdOt7eaQOtY 

This project was to try and make an AI that would follow basic rules to complete a game of minesweeper. The project is separated into 2 main parts, the board and the AI. The board doesn’t have an interaction with the AI and the AI can only perform moves that a normal human player can perform on the board. 
The two actions are `Select` and `Flag`, `Select` will uncover the square and if it’s a bomb, the game ends as a loss. `Flag` will place a flag on the square and if a square has a flag, it will remove the flag. 
The one difference between a human and the AI is navigating the board, a human uses W,A,S,D while the AI uses a special function to skip straight to the square it wants.


## The Board Creation ##
- Create the square board
- Generate X amount of locations on the board for bombs
- Place bombs into the board
- For each bomb, increase the number in the squares around it

Each square has its own ID, the ID can be found by using a squares X and Y position by doing `(xLenght * y) + x`. Using this ID, information about the square can be gotten.

## AI process ##
For each turn, the AI scans board looking for the best move. The best move is a move where it has the highest chance of finding a bomb. This is done by percentages out of 100. The following is done on every square to find out the probability of finding a bomb.
`100 / Amount_of_squares_hidden = Percentage_of_each_hidden_square`
`Percentage_of_each_hidden_square * Current_Square_Number = Percentage_of_finding_all_bombs`
Using this end value, 100% means all bombs around the square can be located and 0% means no chance to find any bombs.

Using this value, the following rules are applied:

1. Perform any moves in move buffer
2. Work out best move based of probability of finding all bombs around a number
	1. 	-If number of flags around square matches square number, add remaining hidden squares to move buffer as "Select"
	2. 	-If flags number is above square number, remove flags
	3. 	-If no flags left, selected all remaining squares
	4. 	-If no probability above 0%, select random square
	5. 	-Use highest probability node for following steps
	6. 	-If 100% probability, place flags on all hidden squares around current
	7. 	-Default move, pick random square around current (LAST RULE)

The move buffer will store any moves found in the last loop. If a square has a number of 2 and only 2 hidden squares around it, this means they have to be bombs. The 2 `Flag` moves can then be placed into the move buffer and then next loop, they're then performed and removed from the buffer.