#include "stdafx.h"
#include "StartScreen.h"


StartScreen::StartScreen()
{
	//Create Box
	string bStr = "";
	for (int y = 0; y < 12; y++) {
		for (int x = 0; x < 35; x++) {
			bStr += " ";
		}
		bStr += "\n";
	}
	bgBox = consoleEngine->CreateText(5, 5, 9, bStr, 136);
	bStr = "";
	for (int y = 0; y < 10; y++) {
		for (int x = 0; x < 31; x++) {
			bStr += " ";
		}
		bStr += "\n";
	}
	box = consoleEngine->CreateText(7, 6, 10, bStr, 119);
	//Create header
	header = consoleEngine->CreateText(12, 7, 12, "   Minesweeper AI\nProgrammed by Jack B", 112);
	//Create options
	string oStr = "";
	for (auto &t : text) {
		oStr += t + "\n";
	}
	optionText = consoleEngine->CreateText(9, 9, 12, oStr, '*',112);
	selector = consoleEngine->CreateText(13, SELECTOR_DEFAULT_Y, 11, "--->           <---", 120);
}

StartScreen::~StartScreen()
{
	consoleEngine->DeleteText(header);
	consoleEngine->DeleteText(optionText);
	consoleEngine->DeleteText(box);
	consoleEngine->DeleteText(bgBox);
	consoleEngine->DeleteText(selector);
}

int StartScreen::Update()
{
	char in = consoleEngine->GetInput();
	if (in == 'w') {
		if (currentSelector > 0) {
			currentSelector--;
			selector->MoveUp(1);
		}
	}
	else if (in == 's') {
		if (currentSelector < 2) {
			currentSelector++;
			selector->MoveDown(1);
		}
	}
	else if (in == 32) {

		return currentSelector;
	}
	return -1;
}

