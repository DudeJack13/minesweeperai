#include "stdafx.h"
#include "Board.h"

#define DEBUG

Board::Board(int xLenght, int yLenght, int amountOfBombs)
{
	infoTextStr = "Place the Flags on Bombs";
	selector = consoleEngine->CreateText(0, 0, 2, "| |", 11);//consoleEngine->CreateObject(0, 0, 2, "|*|", '*', 11);
	bombText = consoleEngine->CreateText(boardTopLeftX + 12, boardTopLeftY - 2, 1, "Bombs: " + to_string(bombs), 15);
	flagText = consoleEngine->CreateText(boardTopLeftX + 1, boardTopLeftY - 2, 1, "Flags: " + to_string(flags), 15);
	moveText = consoleEngine->CreateText(boardTopLeftX + 23, boardTopLeftY - 2, 1, "Moves: " + to_string(moveCounter), 15);
	infoText = consoleEngine->CreateText(boardTopLeftX + 1, boardTopLeftY - 1, 1, infoTextStr, 15);
	bombs = amountOfBombs;
	CreateBoard(xLenght, yLenght);
	Setup(xLenght, yLenght);
}

Board::~Board()
{
	for (auto &square : board) {
		delete(square);
	}
	board.clear();
	bombLocations.clear();
}

void Board::Setup(int xLenght, int yLenght)
{
	//Set random number
	auto seed = static_cast <unsigned int> (time(0)) + moveCounter;
	//auto seed = 1531875033 + moveCounter; //FOR TESTING
	srand(seed);

	//Reset
	for (auto &b : board) {
		b->number = 0;
		b->flagged = false;
		b->bomb = false;
		b->square->SetText("/");
		b->square->SetColour(15);
		b->toBeChecked = false;
		b->visible = false;
	}
	bombLocations.clear();
	infoTextStr = "Place the Flags on Bombs";
	selectionPos = { (xLenght / 2) - 1 , (yLenght / 2) - 1 };
	moveCounter = 0;

	PopulateBoard();
#ifdef DEBUG
	liveLog->logInfo("BOARD: Board loaded with " + to_string(bombLocations.size()) + 
		" bombs | Size: X:" + to_string(xLenght) + " | Y:" + to_string(yLenght) + 
		" | Seed: " + to_string(seed));
#endif
	bombText->SetText("Bombs: " + to_string(bombs));
	flagText->SetText("Flags: " + to_string(flags));
	moveText->SetText("Moves: " + to_string(moveCounter));
	infoText->SetText(infoTextStr);
	UpdateSelectorPos();
}

void Board::CreateBoard(int xLenght, int yLenght)
{
	//Create board
	string tAndBLine = "";
	string innerLines = "";
	string boardLines = "";
	tAndBLine += "+";
	for (int x = 0; x < (xLenght * 4) - 1; x++) { //Top and Bottom lines
		tAndBLine += "-";
	}
	tAndBLine += "+";
	boardLines += tAndBLine + "\n";
	for (int x = 0; x < xLenght + 1; x++) {
		innerLines += "|   ";
	}
	for (int y = 0; y < yLenght; y++) {
		boardLines += innerLines + "\n";
	}
	boardLines += tAndBLine;
	boardObj = consoleEngine->CreateText(boardTopLeftX, boardTopLeftY, 1, boardLines, 15);

	//Create squares
	int id = 0;
	int bombCounter = 0;
	for (int y = 0; y < yLenght; y++) {
		for (int x = 0; x < xLenght; x++) {
			board.push_back(new Square(
				consoleEngine->CreateText((x * 4) + boardTopLeftX + 2, y + boardTopLeftY + 1, 3, "/", 15), id, x, y, false));
			id++;
		}
	}
	this->xLenght = xLenght;
	this->yLenght = yLenght;
}

int Board::GetID(int x, int y)
{
	return (xLenght * y) + x;
}

vector<int> Board::GetSurroundingSquares(int x, int y)
{
	vector<int> surroundingBoxesID;
	if (x - 1 >= 0 && y - 1 >= 0) { //Check top left
		surroundingBoxesID.push_back(GetID(x + squareCheckOffsets[0].first, y + squareCheckOffsets[0].second));
	}
	if (y - 1 >= 0) { //Check top
		surroundingBoxesID.push_back(GetID(x + squareCheckOffsets[1].first, y + squareCheckOffsets[1].second));
	}
	if (x + 1 < xLenght && y - 1 >= 0) { //Check top right
		surroundingBoxesID.push_back(GetID(x + squareCheckOffsets[2].first, y + squareCheckOffsets[2].second));
	}

	if (x - 1 >= 0) { //Left
		surroundingBoxesID.push_back(GetID(x + squareCheckOffsets[3].first, y + squareCheckOffsets[3].second));
	}
	if (x + 1 < xLenght) { //Right
		surroundingBoxesID.push_back(GetID(x + squareCheckOffsets[4].first, y + squareCheckOffsets[4].second));
	}

	if (x - 1 >= 0 && y + 1 < yLenght) { //Check bottom left
		surroundingBoxesID.push_back(GetID(x + squareCheckOffsets[5].first, y + squareCheckOffsets[5].second));
	}
	if (y + 1 < yLenght) { //Check bottom
		surroundingBoxesID.push_back(GetID(x + squareCheckOffsets[6].first, y + squareCheckOffsets[6].second));
	}
	if (x + 1 < xLenght && y + 1 < yLenght) { //Check bottom right
		surroundingBoxesID.push_back(GetID(x + squareCheckOffsets[7].first, y + squareCheckOffsets[7].second));
	}
	return surroundingBoxesID;
}

void Board::PopulateBoard()
{
	//Generate bomb locations
	for (int a = 0; a < bombs; a++) {
		int rX = rand() % xLenght;
		int rY = rand() % yLenght;
		//Check to see if random pos is already taken by another bomb
		if (!board[GetID(rX, rY)]->bomb) {
			bombLocations.push_back(make_pair(rX, rY));
			board[GetID(rX, rY)]->bomb = true;
		}
		else { a--; } //Else loop again	
	}
	flags = bombLocations.size();

	//Generate numbers
	for (auto &bomb : bombLocations) {
		vector<int> surroundingBoxes = GetSurroundingSquares(bomb.first, bomb.second);
		for (auto &box : surroundingBoxes) {
			board[box]->number++;
		}
	}
}

void Board::EndGame(int outcome)
{
	if (outcome == 0) { //Winner
		gameState = 1;
		if (bombs < (xLenght * yLenght) / 4) {
			bombs++;
		}
		infoTextStr = "YOU WIN! Press SPACE to continue!";
	}
	else if (outcome == 1) { //Loser
		gameState = 1;
		infoTextStr = "YOU LOSE! Press SPACE to reset!";
		//Show all bomnbs
		for (auto &bomb : bombLocations) {
			if (!board[GetID(bomb.first, bomb.second)]->flagged) {
				board[GetID(bomb.first, bomb.second)]->square->SetText("B");
				board[GetID(bomb.first, bomb.second)]->square->SetColour(12);
			}
		}
	}
	infoText->SetText(infoTextStr);
}

bool Board::Update()
{
	if (gameState == 0) {
		if (enableControls) {
			return UpdateControls();
		}
		else {
			return true;
		}
	}
	else if (gameState == 1) {
		return EndGameUpdate();
	}
	return false;
}

bool Board::EndGameUpdate()
{
	//Get input
	int in = _getch();
	if (in == 32) { //Spacebar
		gameState = 0;
		Setup(xLenght, yLenght);
	}
	else if (in == 27) { //Escape
		return false;
	}
	return true;
}

bool Board::IfWon()
{
	int bCounter = 0;
	for (auto &bomb : bombLocations) {
		if (board[GetID(bomb.first, bomb.second)]->flagged) {
			bCounter++;
		}
	}
	int vCounter = 0;
	if (bCounter == bombs) {
		for (auto &sqr : board) {
			if (sqr->visible) {
				vCounter++;
			}
		}
	}
	return bCounter == bombs && vCounter == board.size() - bombs;
}

bool Board::UpdateControls()
{
	//Get input
	char in = consoleEngine->GetInput();

	//Deal with input
	if (in == 'w' && selectionPos.second > 0) {
		selectionPos.second -= 1;
		UpdateSelectorPos();
	}
	else if (in == 's' && selectionPos.second < yLenght - 1) {
		selectionPos.second += 1;
		UpdateSelectorPos();
	}
	else if (in == 'd' && selectionPos.first < xLenght - 1) { 
		selectionPos.first += 1;
		UpdateSelectorPos();
	}
	else if (in == 'a' && selectionPos.first > 0) {
		selectionPos.first -= 1;
		UpdateSelectorPos();
	}
	else if (in == 32) { //Space
		Select();
	}
	else if (in == 13) { //Enter
		PlaceFlag();
	}
	else if (in == 27) { //Escape
		return false;
	}
	return true;
}

void Board::PlaceFlag()
{
	int id = GetID(selectionPos.first, selectionPos.second);
	if (!board[id]->flagged && flags > 0) {//Flag sqaure
		moveCounter++;
		board[id]->flagged = true;
		board[id]->square->SetText("X");
		board[id]->square->SetColour(10);
		flags--;
		if (flags == 0) {
			if (IfWon()) {
				EndGame(0);
			}
		}
	}
	else if (board[id]->flagged) { //Unflag
		moveCounter++;
		board[id]->flagged = false;
		board[id]->square->SetText("/");
		board[id]->square->SetColour(15);
		flags++;
	}
	moveText->SetText("Moves: " + to_string(moveCounter));
	flagText->SetText("Flags: " + to_string(flags));
}

void Board::Select()
{
	int id = GetID(selectionPos.first, selectionPos.second);
	if (!board[id]->visible && !board[id]->flagged) {
		board[id]->visible = true;
		if (board[id]->number != 0) {
			board[id]->square->SetText(to_string(board[id]->number));
		}
		else {
			board[id]->square->SetText(" ");
		}
		//If blank space
		if (board[id]->bomb) {
			EndGame(1); 
		}
		else if (board[id]->number == 0) {
			vector<int> toCheckID = { id };
			while (!toCheckID.empty()) {
				vector<int> surroundingBoxes = GetSurroundingSquares(board[toCheckID.front()]->x, board[toCheckID.front()]->y);
				for (auto &sB : surroundingBoxes) {
					if (board[sB]->number == 0 && !board[sB]->visible) {
						toCheckID.push_back(sB);
					}
					board[sB]->visible = true;
					if (board[sB]->number == 0) {
						board[sB]->square->SetText(" ");
					}
					else {
						board[sB]->square->SetText(to_string(board[sB]->number));
					}
					
				}
				toCheckID.erase(toCheckID.begin());
			}
		}
		moveCounter++;
	}
	moveText->SetText("Moves: " + to_string(moveCounter));

	if (flags == 0) {
		if (IfWon()) {
			EndGame(0);
		}
	}
}

void Board::UpdateSelectorPos()
{
	int startSqur = GetID(selectionPos.first, selectionPos.second);
	selector->SetPosition(board[startSqur]->square->GetX() - 1, board[startSqur]->square->GetY());
}

Board::Square* Board::GetSqaureInfo(int id)
{
	return board[id];
}

void Board::SetSelector(int x, int y)
{
	selectionPos.first = x;
	selectionPos.second = y;
	UpdateSelectorPos();
}

int Board::GetWidth()
{
	return xLenght;
}

int Board::GetHeight()
{
	return yLenght;
}

void Board::EnableControls(bool b)
{
	enableControls = b;
}

vector<Board::Square*> Board::GetBoard()
{
	return board;
}

int Board::GetBoardState()
{
	return gameState;
}

int Board::GetAmountOfFlags()
{
	return flags;
}