#include "stdafx.h"
#include <iostream>
#include <windows.h>
#include <string>

#include "Board.h"
#include "StartScreen.h"
#include "LiveLogger.h"
#include "AISolver.h"
#include "ConsoleEngine.h"

#define DEBUG

using namespace consoleengine;

//Global varaibles
LiveLogger* liveLog = NULL;
Board* board = NULL;
AISolver* aiSolver = NULL;
ConsoleEngine* consoleEngine = new ConsoleEngine(405,480);

int main(int argc, char* argv[])
{
	consoleEngine->SetBounds(45,26);
	//Set up logger if in debug mode
#ifdef DEBUG
	liveLog = new LiveLogger(argv[0]);
#endif
	StartScreen* startScreen = new StartScreen();
	board = new Board(10, 20, 5);

	int gameState = 0;
	while (consoleEngine->Running()) {
		
		if (gameState == 0) {
			int startReturn = startScreen->Update();
			if (startReturn == 0) { //Player plays
				board->EnableControls(true);
				gameState = 1;
				startScreen->~StartScreen();
				delete startScreen;
			}
			else if (startReturn == 1) { //AI plays
				board->EnableControls(false);
				aiSolver = new AISolver();
				gameState = 1;
				startScreen->~StartScreen();
				delete startScreen;
			}
			else if (startReturn == 2) { //Exit
				consoleEngine->ExitEngine();
			}
		}
		else if (gameState == 1) {
			if (board->Update()) {
				if (aiSolver != NULL) {
					aiSolver->Update();
				}
			}
			else {
				consoleEngine->ExitEngine();
			}
		}
	}

#ifdef DEBUG
	liveLog->logInfo("Ending program, starting unloading");
#endif 

	if (board != NULL) { board->~Board(); }
	if (aiSolver != NULL) { aiSolver->~AISolver(); }

#ifdef DEBUG
	liveLog->~LiveLogger();
#endif

	return 0; //Quit
}