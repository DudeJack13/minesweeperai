#pragma once
#ifndef BOARD_H
#define BOARD_H

#include <vector>
#include <iostream>
#include <string>
#include <windows.h>
#include <cstdlib>
#include <ctime>
#include <conio.h> 

#include "LiveLogger.h"
#include "ConsoleEngine.h"

using namespace consoleengine;

extern LiveLogger* liveLog;
extern ConsoleEngine* consoleEngine;

class Board
{
private:

	struct Square
	{
		Square(Text* square, int _id, int _x, int _y, bool isBomb) {
			id = _id;
			x = _x;
			y = _y;
			bomb = isBomb;
			this->square = square;
		}
		int x;
		int y;
		int id;
		bool bomb = false;
		bool visible = false;
		int number = 0;
		bool flagged = false;
		bool toBeChecked = false;
		Text* square;
	};

	vector<Square*> board;
	Text* boardObj = NULL;
	Text* selector = NULL;
	pair<int, int> selectionPos = { 0,0 };
	Text* bombText = NULL;
	Text* flagText = NULL;
	Text* infoText = NULL;
	Text* moveText = NULL;


	//Board creation varaibles
	int xLenght;
	int yLenght;
	const int squareSize = 4;
	const int boardTopLeftX = 2;
	const int boardTopLeftY = 3;
	int flags;
	int gameState = 0;
	int bombs = 0;
	string infoTextStr;
	vector <pair<int, int>> bombLocations;
	bool enableControls;
	int moveCounter;

	const vector<pair<int, int>> squareCheckOffsets = {
		{-1,-1},	//Top left	
		{0,-1},		//Top
		{1,-1},		//Top right
		{-1,0},		//Left
		{1,0},	   //Right
		{-1,1},	   //Bottom left
		{0,1},    //Bottom
		{1,1},    //Bottom right
	};

	//Selecting
	void UpdateSelectorPos();

	//Game functions
	void Setup(int xLenght, int yLenght);
	void PopulateBoard();
	bool UpdateControls();

	//Board functions
	void CreateBoard(int xLenght, int yLenght);
	bool IfWon();

	//End Game
	void EndGame(int outcome);
	bool EndGameUpdate();

	//Helper functions
	int GetID(int x, int y);

public:
	Board() {}
	Board(int xLenght, int yLenght, int AmountOfBombs);
	~Board();
	bool Update();

	//For AI to interact
	vector<Square*> GetBoard();
	Square* GetSqaureInfo(int id);
	vector<int> GetSurroundingSquares(int x, int y);
	void SetSelector(int x, int y);
	void PlaceFlag();
	void Select();
	int GetWidth();
	int GetHeight();
	int GetBoardState();
	int GetAmountOfFlags();
	void EnableControls(bool b);
};

#endif // !BOARD_H

