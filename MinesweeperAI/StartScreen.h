#pragma once
#ifndef STARTSCREEN_H
#define STARTSCREEN_H

#include <iostream>
#include <string>
#include <windows.h>
#include <conio.h> 

#include "ConsoleEngine.h"

using namespace consoleengine;
extern ConsoleEngine* consoleEngine;

class StartScreen
{
private:
	Text * header = NULL;
	Text * optionText = NULL;
	Text * box = NULL;
	Text * bgBox = NULL;
	Text * selector = NULL;
	int currentSelector = 0;
	const int SELECTOR_DEFAULT_Y = 11;

	string text[6] = {
		"---------------------------",
		"***Select Option Number",
		"*********PLAY SOLO",
		"**********AI MODE",
		"***********QUIT",
		"---------------------------"
	};
public:
	StartScreen();
	~StartScreen();
	int Update();
};

#endif // !STARTSCREEN_H