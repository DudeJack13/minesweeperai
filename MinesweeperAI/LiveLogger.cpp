#include "stdafx.h"
#include "LiveLogger.h"

LiveLogger::LiveLogger(string str)
{
	dir = str.substr(0, str.find_last_of("\\/"));
	wholeDir = str;
	logInfo(wholeDir + " is now logging");
}

void LiveLogger::_SendMessage(string str)
{
	ofstream file;
	file.open(dir + "\\PythonLogger\\logFile.txt", fstream::app);
	file << str << "\n";
	file.close();
}
