#include "stdafx.h"
#include "AISolver.h"

#define DEBUG

AISolver::AISolver()
{
	//srand(static_cast <unsigned int> (time(0)));
#ifdef DEBUG
	liveLog->logInfo("AI Solver Loaded");
#endif
}

bool AISolver::Update()
{
	//If board isn't in play mode
	if (board->GetBoardState() != 0) {
		return true;
	}

	//NOTES:
	//Work out probability of finding all bombs around square
		//EXAMPLE:
		// |   |   |   |
		// | 2 ||3|| 2 |
		// | / | / | / |
	//Find out how many visible around (5) Leaving x amount invisible (3)
	//100% chance of finding all bombs

		//EXAMPLE:
		// |   |   |   |
		// | 2 ||2|| 2 |
		// | / | / | / |
	//Find out how many visible around (5) Leaving x amount invisible (3)
	//66% chance of finding bomb all bomb 

	//How to get percentage
	//Out of 100%
	//100 / amount hidden = percentage of each hidden sqaure
	//percentage of each hidden sqaure * current square number = percentage of finding all bombs

	//Process
	//1) Perform any moves in move buffer 
	// ----- Perform process in order -----
	//2) Work out best move based of probability of finding all bombs around a number
		//-If number of flags around square matches square number, add remaining hidden
		//	squares to move buffer as "Select"
		//-If flags number is above square number, remove flags
		//-If no flags left, selected all remaining squares
		//-If no probability above 0%, select random square
		//-Use highest probability node for following steps
			//-If 100% probability, place flags on all hidden squares around current
			//-Default move, pick random square around current (LAST RULE)

	//------------------------------------------------------------
	//1) Perform any moves in move buffer
	//------------------------------------------------------------
	if (!moveBuffer.empty()) {
		board->SetSelector(moveBuffer.front()->_x, moveBuffer.front()->_y);
		if (moveBuffer.front()->_move == Select) {
			board->Select();
#ifdef DEBUG
			liveLog->logInfo("AI Move: x:" + to_string(moveBuffer.front()->_x) +
				" y: " + to_string(moveBuffer.front()->_y) + " : Move: Select");
#endif
		}
		else if (moveBuffer.front()->_move == Flag) {
			board->PlaceFlag();
#ifdef DEBUG
			liveLog->logInfo("AI Move: x:" + to_string(moveBuffer.front()->_x) +
				" y: " + to_string(moveBuffer.front()->_y) + " : Move: Flag");
#endif
		}
		delete(moveBuffer.front());
		moveBuffer.erase(moveBuffer.begin());
		return true;
	}

	//------------------------------------------------------------
	//2) Work out moves based of probability of finding all bombs around a number
	//------------------------------------------------------------
	IDs* bestMoveID = new IDs();
	IDs* idTemp = new IDs();
	//For current square, find the probability of finding all bombs
	for (auto &square : board->GetBoard()) {
		//If the square has a number in it and is visible
		if (square->visible && square->number > 0) {
			vector <int> surroundSqaures = board->GetSurroundingSquares(square->x, square->y);
			//Find how many hidden bombs around square
			int amountHidden = 0;
			int amountFlagged = 0;
			for (auto &sSqaure : surroundSqaures) {
				if (board->GetSqaureInfo(sSqaure)->flagged) {
					amountFlagged++;
					amountHidden++;
				}
				else if (!board->GetSqaureInfo(sSqaure)->visible) {
					amountHidden++;
				}
			}
			//------------------------------------------------------------
			//-If number of flags around square matches square number, add remaining hidden
			//	squares to move buffer as "Select"
			//------------------------------------------------------------
			if (square->number == amountFlagged && amountHidden > 0) {
				for (auto &sSquare : surroundSqaures) {
					if (!board->GetSqaureInfo(sSquare)->visible && !board->GetSqaureInfo(sSquare)->flagged) {
						moveBuffer.push_back(new MoveBuffer(
							board->GetSqaureInfo(sSquare)->x, board->GetSqaureInfo(sSquare)->y, Select)
						);
					}
				}
				if (!moveBuffer.empty()) {
					delete(idTemp);
					return true;
				}
			}
			//------------------------------------------------------------
			//-If flags number is above square number, remove flags
			//------------------------------------------------------------
			else if (square->number < amountFlagged) {
				for (auto &sSquare : surroundSqaures) {
					if (board->GetSqaureInfo(sSquare)->flagged) {
						moveBuffer.push_back(new MoveBuffer(
							board->GetSqaureInfo(sSquare)->x, board->GetSqaureInfo(sSquare)->y, Flag));
					}
				}
				if (!moveBuffer.empty()) {
					delete(idTemp);
					return true;
				}
			}

			else if (square->number != amountFlagged) {
				//Work out probability percentage
				float PercentageOfEachSquare = 100.0f / (float)amountHidden;
				//Probability of knowing where the bomb is
				idTemp->_probability = PercentageOfEachSquare * (float)square->number;
				idTemp->_id = square->id;
				//On first loop, just set bestMoveID to idTemp
				if (idTemp->_probability > bestMoveID->_probability) { //If probability is better
					PassData(bestMoveID, idTemp);
				}
			}
		}
	}
	delete(idTemp);

	//------------------------------------------------------------
	//-If no flags left, selected all remaining squares
	//------------------------------------------------------------
	if (board->GetAmountOfFlags() == 0) {
		for (auto &square : board->GetBoard()) {
			if (!square->visible && !square->flagged) {
				moveBuffer.push_back(new MoveBuffer(square->x, square->y, Select));
			}
		}
		return true;
	}

	//------------------------------------------------------------
	//-If no probability above 0% , select random square
	//------------------------------------------------------------
	if (bestMoveID->_probability <= 50.0f) {
		int x = rand() % board->GetWidth();
		int y = rand() % board->GetHeight();
		moveBuffer.push_back(new MoveBuffer(x, y, Select));
		return true;
	}

	//------------------------------------------------------------
	//-Use highest probability node for following steps
	//------------------------------------------------------------

	//------------------------------------------------------------
	//-If 100% probability, place flags on all hidden squares around current
	//------------------------------------------------------------
	if (bestMoveID->_probability >= 99.0f) {
		vector <int> surroundSqaures = board->GetSurroundingSquares(
			board->GetSqaureInfo(bestMoveID->_id)->x,
			board->GetSqaureInfo(bestMoveID->_id)->y);
		for (auto &square : surroundSqaures) {
			if (!board->GetSqaureInfo(square)->visible && !board->GetSqaureInfo(square)->flagged) {
				moveBuffer.push_back(new MoveBuffer(
					board->GetSqaureInfo(square)->x,
					board->GetSqaureInfo(square)->y,
					Flag)
				);
			}
		}
		return true;
	}

	//------------------------------------------------------------
	//-Default move, pick random sqaure around current (LAST RULE)
	//------------------------------------------------------------
	//If code got to here, all rules have been skipped 
#ifdef DEBUG
	liveLog->logWarning("AI Warning: No rules hit, Choosing random square with highest chance");
#endif
	vector <int> surroundSqaures = board->GetSurroundingSquares(
		board->GetSqaureInfo(bestMoveID->_id)->x,
		board->GetSqaureInfo(bestMoveID->_id)->y);
	vector<int> ids;
	for (auto &square : surroundSqaures) {
		if (!board->GetSqaureInfo(square)->visible && !board->GetSqaureInfo(square)->flagged) {
			ids.push_back(square);
		}
	}
	int randNum = rand() % ids.size(); 
	moveBuffer.push_back(new MoveBuffer(
		board->GetSqaureInfo(ids[randNum])->x, board->GetSqaureInfo(ids[randNum])->y, Select)
	);

	//Return default true
	return true;
}

void AISolver::PassData(IDs* id1, IDs* id2)
{
	id1->_id = id2->_id;
	id1->_probability = id2->_probability;
	id2->_id = -1;
	id2->_probability = -1;
}
