#pragma once
#ifndef AISOLVER_H
#define AISOLVER_H

#include "LiveLogger.h"
#include "Board.h"

#include <iostream>
#include <vector>
#include <cstdlib>
#include <ctime>
#include <Windows.h>

extern LiveLogger* liveLog;
extern Board* board;

using namespace std;

class AISolver
{
private:
	enum Moves {
		Select, Flag
	};

	struct IDs
	{
		int _id = -1;
		float _probability = -1;
	};

	struct MoveBuffer
	{
		MoveBuffer(int x, int y, Moves move) {
			_x = x;
			_y = y;
			_move = move;
		}
		int _x;
		int _y;
		Moves _move;
	};
	vector <MoveBuffer*> moveBuffer;
	//Helper function
	void PassData(IDs* id1, IDs*  id2);
public:
	AISolver();
	~AISolver() {}
	bool Update();
};

#endif 